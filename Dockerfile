FROM openjdk:8-jdk-alpine

RUN apk add --no-cache imagemagick bash

WORKDIR /app

COPY ./target/ /app/target


COPY thumbnail.sh /app/target/thumbnail.sh

COPY entry-point.sh /app/target/entry-point.sh

RUN chmod +x /app/target/entry-point.sh
RUN chmod +x /app/target/thumbnail.sh

WORKDIR /app/target

ENV TN_SIZE=150

ENTRYPOINT ["./entry-point.sh"]