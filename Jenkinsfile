pipeline {
  agent any
  options {
    timestamps()
    timeout(time: 5, unit: 'MINUTES')
  }
  triggers {
    gitlab(triggerOnPush: true)
  }
  tools {
    jdk 'jdk8'
    maven 'maven3.6.2'
  }

  stages {

    stage('Compile') {
      steps {
        sh 'mvn compile'
      }
    }

    stage('Unit test') {
      steps {
        sh 'mvn test'
      }
    }

    stage('Package') {
      steps {
        sh 'mvn package'
      }
    }

    stage('E2E Test') {
      steps {
        sh 'mvn verify'
      }
    }

    stage('Publish to m2') {
      steps {
        configFileProvider(
          [configFile(fileId: 'e374c569-a9a8-4d73-bfd8-f8429ef13150', variable: 'MAVEN_SETTINGS')]) {

          sh 'mvn -s $MAVEN_SETTINGS install'
        }
      }
    }
  }
}